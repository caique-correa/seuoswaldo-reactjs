import teste from './modules/Financeiro/teste';
import Documentation from './Documentation';

export const Rotas = [
    {
        path: '/',
        component: Documentation
    },
    {
        path: '/teste',
        component: teste
    }
]
